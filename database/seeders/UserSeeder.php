<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Jimmy Morgan',
            'email' => 'jimmymorgan@yahoo.com',
            'password' => Hash::make('Password0'),
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Derrick Ontiveros',
            'email' => 'derrick@miashare.com',
            'password' => Hash::make('Password1'),
        ]);
    }
}
