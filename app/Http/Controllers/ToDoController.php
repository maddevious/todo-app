<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ToDo;

class ToDoController extends Controller
{
    public function index()
    {
        $is_admin = auth()->user()->is('admin');
        if ($is_admin) {
            $todos = ToDo::withTrashed()->get();
        } else {
            $todos = auth()->user()->todos()->get();
        }
        return view('todo.index', [
            'todos' => $todos,
            'is_admin' => $is_admin,
        ]);
    }

    public function create()
    {
        return view('todo.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required'
        ]);
        $todo = new ToDo();
        $todo->description = $request->description;
        $todo->user_id = auth()->user()->id;
        $todo->save();
        $request->session()->flash('status', json_encode([
            'type' => 'success',
            'message' => 'To Do was successfully saved',
        ]));
        return redirect('/todos');
    }

    public function edit($id)
    {
        $todo = auth()->user()->todos()->where('id', $id)->first();
        if (!empty($todo))
        {
            return view('todo.edit', [
                'todo' => $todo
            ]);
        }
        else {
            return redirect('/todos');
        }
    }

    public function update(Request $request, $id)
    {
        $todo = auth()->user()->todos()->where('id', $id)->first();
        if(isset($_POST['delete'])) {
            $todo->delete();
            $request->session()->flash('status', json_encode([
                'type' => 'success',
                'message' => 'To Do was successfully deleted',
            ]));
        } else {
            $this->validate($request, [
                'description' => 'required'
            ]);
            $todo->description = $request->description;
            $todo->is_complete = $request->is_complete;
            $todo->save();
            $request->session()->flash('status', json_encode([
                'type' => 'success',
                'message' => 'To Do was successfully saved',
            ]));
        }
        return redirect('/todo/'.$id);
    }
}
