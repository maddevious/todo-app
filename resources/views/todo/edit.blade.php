<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit To Do') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                <form method="POST" action="/todo/{{ $todo->id }}">
                    @include('layouts.status', ['status' => request()->session()->get('status')])
                    <div class="form-group">
                        <textarea name="description" class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3 font-medium placeholder-gray-700 focus:outline-none focus:bg-white">{{$todo->description }}</textarea>
                        @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                        <input class="form-check-input" type="checkbox" id="is_complete" name="is_complete"
                               value="1" {{ !empty($todo->is_complete) ? 'checked='.'"'.'checked'.'"' : '' }}> Complete?</input>
                    </div>

                    <div class="form-group  mt-3">
                        <button type="submit" name="update" class="bg-gray-900 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Update task</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
