<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('To Do List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                @include('layouts.status', ['status' => request()->session()->get('status')])
                <div class="flex">
                    <div class="flex-auto text-2xl mb-4"></div>
                    <div class="flex-auto text-right mt-2">
                        <a href="/todo" class="bg-gray-900 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">New</a>
                    </div>
                </div>
                <table class="w-full text-md rounded mb-4">
                    <thead>
                    <tr class="border-b">
                        <th class="text-left p-3 px-5">Task</th>
                        @if ($is_admin)
                            <th class="text-left p-3 px-5">User</th>
                        @endif
                        <th class="text-left p-3 px-5">Created</th>
                        <th class="text-left p-3 px-5">Actions</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($todos as $todo)
                        <tr class="border-b hover:bg-orange-100">
                            <td class="p-3 px-5 {{ !empty($todo->is_complete) ? 'line-through' : '' }}">
                                {{ $todo->description }}
                            </td>
                            @if ($is_admin)
                                <td class="p-3 px-5 {{ !empty($todo->is_complete) ? 'line-through' : '' }}">
                                    {{ $todo->user->name }}
                                </td>
                            @endif
                            <td class="p-3 px-5 {{ !empty($todo->is_complete) ? 'line-through' : '' }}">
                                {{ $todo->created_at->diffForHumans() }}
                            </td>
                            <td class="p-3 px-5">
                                @if ($todo->user_id == auth()->user()->id)
                                <a href="/todo/{{$todo->id}}" name="edit" class="mr-3 text-sm bg-gray-900 hover:bg-gray-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Edit</a>
                                    @if (!$todo->trashed())
                                    <form action="/todo/{{$todo->id}}" class="inline-block">
                                        <button type="submit" name="delete" formmethod="POST" class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Delete</button>
                                        {{ csrf_field() }}
                                    </form>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</x-app-layout>
