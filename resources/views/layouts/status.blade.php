@php
if (!empty($status)) {
    $statusObj = json_decode($status);
    if ($statusObj->type == 'success') {
        $color = 'green';
    } else {
        $color = 'red';
    }
    echo '<div class="p-5 mb-5 text-'.$color.'-700 bg-'.$color.'-100">'.$statusObj->message.'</div>';
}
@endphp
