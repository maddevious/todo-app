<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ToDoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/todos');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/todos',[ToDoController::class, 'index'])->name('index');
    Route::get('/todo',[ToDoController::class, 'create']);
    Route::post('/todo', [ToDoController::class, 'store']);
    Route::get('/todo/{todo}', [ToDoController::class, 'edit']);
    Route::post('/todo/{todo}', [ToDoController::class, 'update']);
});

require __DIR__.'/auth.php';
